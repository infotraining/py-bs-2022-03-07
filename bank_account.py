# class BankAccount
# owner
# balance
# withdraw()
# deposit()
# status()

class BankAccount:
    __interest = 0.01

    def __init__(self, owner="John", balance=0):
        self.__owner = owner
        self.__balance = balance

    def withdraw(self, value):
        self.__balance -= value

    def deposit(self, value):
        self.__balance += value

    def status(self):
        print("Owner: ", self.__owner, ":: Balance: ", self.__balance)

    def __repr__(self):
        return "<O: {} :: B: {}>".format(self.__owner, self.__balance)

    @staticmethod
    def get_bank_name():
        print("HtB Bank")

    @classmethod
    def get_interest_rate(cls):
        return cls.__interest

owner1 = BankAccount(balance=100)
owner2 = BankAccount("Mary", 200)
print("Before:")
owner1.status()
owner2.status()

owner1.deposit(100)
owner2.withdraw(100)

print("Print: ")
print(owner1)

print("After:")
owner1.status()
owner2.status()
BankAccount.get_bank_name()
print(BankAccount.get_interest_rate())
