# print primes in range(2, 100)

# % modulo - division reminder
# for loop
# if condition

# for i in range(2, 100):
#     print(i)


for i in range(2, 100):
    is_prime = True
    for div in range(2, i):
        if i % div == 0:
            is_prime = False
            break

    if is_prime:
        print(i, "is prime")
    else:
        pass
        #print(i, "is NOT prime")

print("end")

for i in range(2, 100):
    for div in range(2, i):
        if i % div == 0:
            break
    else:    # if not breaked in for loop
        print(i, "is prime!!")

print("end")