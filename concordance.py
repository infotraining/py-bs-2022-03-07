# calculate frequency of words in a text

#text = open("swann.txt", "r", encoding="utf8").read()
text = open("swann.txt", "rb").read().decode('utf8')
# print(text[:100])

words = text.split()
print(len(words))

d = {}
for word in words:
    word = word.lower()
    word = word.strip('''.,:;-?!#*()"''')
    # try:
    #     d[word] += 1
    # except KeyError:
    #     d[word] = 1
    if word not in d:
        d[word] = 1
    else:
        d[word] += 1

print(len(d))
#print(d)

# d = {"the" : 3, "and" : 2, "to" : 2}

# final dict - keys - frequency, values - list of words
if "the" in d:
    print("the:", d["the"])

freq = {}
for word, f in d.items():
    if f not in freq:
        freq[f] = [word]
    else:
        freq[f].append(word)

print(len(freq))
frequencies = list(freq.keys())
frequencies.sort(reverse=True)
for i in frequencies[:15]:
    print(i, freq[i])


"""
# output:
734: the
533: a
234: you

"""