import primes_fun
import primes_fun as p
from primes_fun import print_primes

# primes_fun.print_primes(10)
# p.print_primes(10)
# print_primes(10)

def test_if_is_prime_for_7():
    assert p.is_prime(7) == True

def test_if_is_prime_for_2():
    assert p.is_prime(2) == True

def test_if_is_prime_for_11():
    assert p.is_prime(11) == True

def test_primes_check_for_10_len():
    assert len(p.primes(10)) == 4

def test_primes_check_for_10():
    assert p.primes(10) == [2, 3, 5, 7]
