l = list(range(10))

print(l)

l2 = []
for i in l:
    l2.append(i + 1)

print(l2)

def inc(a):
    return a + 1

def add(a, b):
    return a + b

add = lambda a, b : a + b

l3 = list(map(inc, l2))
print(l3)

l4 = list(map(add, l, l2))
print(l4)

b = 3
l3 = list(map(lambda a : a * b, l2))
print(l3)
b = 4
l3 = list(map(lambda a : a * b, l2))
print(l3)

l4 = list(map(lambda a, b : a + b, l, l2))
print(l4)

f = list(filter(lambda a : a % 2 == 0, l))
print(f)

f = []
for el in l:
    if el % 2 == 0:
        f.append(el)

print(f)

# list comprehension
print("**** list comprehension ****")
l2 = []
for el in l:
    l2.append(el ** 2)

print(l2)
l3 = [el ** 2 for el in l if el % 2 == 0]
print(l3)

## calculate sum of decimal numbers of 2**1000
num = str(2 ** 1000)
s = 0
for digit in num:
    s += int(digit)
print(s)

digits = [int(digit) for digit in str(2**1000)]
#print(digits)

print( sum( int(digit) for digit in str(2**1000) ))

l = [1,3,4]
l2 = [2, 4 ,6]
for i in zip(l, l2):
    print(i)