def fibonacci(n: int) -> list[int]:
    if n < 1:
        raise ValueError("n must be >= 1")
    res = []
    if n == 1:
        res = [1]
    if n == 2:
        res = [1, 1]
    if n > 2:
        res = [1, 1]
        for _ in range(n-2):
            res.append(res[-1] + res[-2])
    return res

fibonacci(2)