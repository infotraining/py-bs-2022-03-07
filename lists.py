a = [1, 2, 3]
print(a)

for i in a:
    print(i)

for i in range(len(a)):
    print(a[i])

print("*" * 10)
print(a[0])
print(a[1])
print(a[2])

a.append(4)
print(a)

a.extend([5, 6, 7])
print(a)

a.append("Leszek")
print(a)

a.insert(0, "Hello")
print(a)

b = [3, 2, 5, 11]
print(id(b))
b.sort()
print(id(b))
print(b)

b[3] = 123
print(id(b))
print(b)

s = "Leszek"
# s[3] = 'g' - does not work

c = b   # only labels, not copy!!!
print(c)
c[3] = 1231231
print(b)

l1 = list(range(10))
print(l1[-1])
print(l1[-2])
print(l1[:5])
print(l1[5:])
print(l1[-3:])
print(l1[:])

l2 = l1[:]
print(id(l1), id(l2))
l2[1] = 123
print(l1)
l1.sort(reverse=True)
print(l1)

