def add(a, b):
    """adds a and b

    >>> add(3, 5)
    8"""
    return a + b

result = add(3, 4)
# print(result)

def is_prime(n):
    """checks if n is prime

    >>> is_prime(2)
    True
    >>> is_prime(3)
    True
    >>> is_prime(6)
    False
    """
    for div in range(2, n):
        if n % div == 0:
            return False
    return True

def print_primes(n):
    """prints primes from 2 to n"""
    for i in range(2, n):
        if is_prime(i):
            print(i)

def primes(n):
    """return primes from 2 to n"""
    p = []
    for i in range(2, n):
        if is_prime(i):
            p.append(i)
    return p

if __name__ == "__main__":
    print("running tests")
    import doctest
    doctest.testmod()
