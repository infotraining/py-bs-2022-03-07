import pytest
# vector on 2D plane
# v = Vector(1, 2)
# v3 = v1.add(v2)
# v3 = v1.mul(4)
# print(v3) - Vector(4, 8)
# v3 = v1 + v2
# v3 = v1 * 4
# v.length # length of vector, distance from origin
# v.x, v.y # x and y coordinates

from vector import Vector

def test_create_vector():
    assert Vector(1, 2) != None

def test_repr_output():
    assert repr(Vector(1, 2)) == "<Vector(1, 2)>"

def test_adding_vectors():
    v1 = Vector(1, 2)
    v2 = Vector(3, 4)
    v3 = v1 + v2
    assert v3 == Vector(4, 6)  # this requires eq

def test_adding_vectors_3d():
    v1 = Vector(1, 2, 0)
    v2 = Vector(3, 4, 2)
    v3 = v1 + v2
    assert v3 == Vector(4, 6, 2)  # this requires eq

def test_eq_vector():
    v1 = Vector(1, 2)
    v2 = Vector(1, 2)
    assert v1 == v2

def test_not_eq_vector():
    v1 = Vector(1, 2)
    v2 = Vector(2, 2)
    assert v1 != v2

def test_multiplying_vector():
    v1 = Vector(1, 2)
    v3 = v1 * 4
    assert v3 == Vector(4, 8)

def test_multiplying_vector_3d():
    v1 = Vector(1, 2, 3)
    v3 = v1 * 4
    assert v3 == Vector(4, 8, 12)


def test_length():
    v1 = Vector(0, 2)
    assert v1.length == 2

# write test for vector length
# DONE write tests for adding and multiplying vectors
