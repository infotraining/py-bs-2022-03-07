## pip install pytest

##  pytest --assert=plain

def add(a, b):
    """Adds a to b
    and returns something"""
    return a + b

print(type(add))
a = add
print(type(a))
print(a.__name__)
print(a(3,4))

print(add(b=5, a=3))

def printname(name="John", surname="Doe"):
    print(name, surname)

printname(name="Leszek", surname="Tarkowski")
printname(name="Piotr")
printname()
printname(surname="Smith")

def fullfunction(name, surname="Doe", age=1):
    print(name, surname, age)

fullfunction("Leszek", "Tarkowski")
fullfunction("John", age=33)

def printer(a, *args, **kwargs):
    print(a)
    print(args)
    print(kwargs)

printer(1)
printer("a", "b", "c", name="Leszek", age=45)

def suma(a=0, *args):
    """takes any number of arguments and return its sum"""
    for i in args:
        a += i
    return a

print(suma(1,2,3,4,5,6,7,8))
print(suma("a", "b", "c"))

params = 1, 2
print(add(*params))
dparams = {"a": 1, "b": 2}
print(add(**dparams))