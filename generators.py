l = [1, 2, 3]
for elem in l:
    print(elem)

for i in range(10):
    print(i)

print(range(10))
l = list(range(4))
print(l)

def gen1():
    yield 1
    yield 2

for i in gen1():
    print(i)

def countdown(n):
    while n > 0:
        yield n
        n -= 1

print(list(countdown(10)))

for i in countdown(5):
    print(i)

print([x**2 for x in countdown(20)])

print( sum(int(x) for x in str(2**1000)))