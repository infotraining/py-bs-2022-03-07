class A:
    def __init__(self):
        super().__init__()
        self.attr = 0
        print("A.__init__")

    def method(self):
        print("A.method")

    def Amethod(self):
        print("A.Amethod")

class D:
    def __init__(self):
        super().__init__()
        print("D.__init__")
class B(A, D):
    def __init__(self):
        super().__init__()
        print("B.__init__")

    def method(self):
        print("B.method")

a = A()
a.method()
print(a.attr)
print("********************")
b = B()
b.method()
b.Amethod()
print(b.attr)