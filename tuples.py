from re import A


t = 1, 2 ,3

print(t)
for el in t:
    print(el)

# t.append(4)

for i, el in enumerate(t):
    print(i, el)

a = 3
b = 4
a, b = b, a
print(a, b)

def divide_with_reminder(a, b):
    res =  a // b
    rem = a % b
    return res, rem

res = divide_with_reminder(10, 3)
print(res)

res, reminder = divide_with_reminder(10, 3)
print(f"{res} {reminder}")

l = list(t)
print(l)

res, _ = divide_with_reminder(15, 2)
print(res)

l = "Ala ma kota"
a, *b, c = l.split()
print(a, b, c)
