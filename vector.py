class Vector:
    def __init__(self, *args):
        self.__pos = args
        self.__len = None

    def __add__(self, other):
        #params = map(lambda x, y: x + y, self.pos, other.pos)
        params = [sum(a) for a in zip(self.pos, other.pos)]
        return Vector(*params)

    def __mul__(self, other):
        return Vector(*[p * other for p in self.pos])

    def __eq__(self, other):
        if not isinstance(other, Vector):
            return False
        return self.pos == other.pos

    def __repr__(self):
        return f"<Vector{self.pos}>"

    def __set_pos(self, pos):
        self.__pos = pos
        self.__len = None

    def __get__pos(self):
        return self.__pos

    pos = property(fget=__get__pos, fset=__set_pos)

    def __length(self):
        if self.__len is None:
            self.__len = sum(x ** 2 for x in self.pos) ** 0.5
        return self.__len

    length = property(fget=__length)

v = Vector(1, 2)
print(v)