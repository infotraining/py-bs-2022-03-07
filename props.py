class MyClass:
    def __init__(self, value) -> None:
        self.__value = value

    def get_value(self):
        return self.__value

    def set_value(self, v):
        if v > 0:
            self.__value = v

class PropClass:
    def __init__(self, value):
        self.__value = value

    def __get_value(self):
        return self.__value

    def __set_value(self, v):
        if v > 0:
            self.__value = v

    def __get_id(self):
        return "PropClass"

    value = property(fget=__get_value, fset=__set_value, fdel=None)
    id = property(fget=__get_id)

c = MyClass(100)
print(c.get_value())
c.set_value(200)
print(c.get_value())

p = PropClass(100)
print(p.value)
p.value = -200
print(p.value)
print(p.id)
p.id = "new id"