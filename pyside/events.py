import PySide6.QtWidgets as Qw
import PySide6.QtCore as Qc
import PySide6.QtGui as Qg
import sys

class Canvas(Qw.QLabel):
    def __init__(self):
        super().__init__()
        self._pixmap = Qg.QPixmap(1000, 1000)
        self.setPixmap(self._pixmap)

    def mouseMoveEvent(self, ev):
        pos = ev.position()
        print(pos.x(), pos.y())
        painter = Qg.QPainter(self._pixmap)
        p = painter.pen()
        p.setColor(Qg.QColor(0, 0, 0))
        p.setWidth(10)
        painter.setPen(p)
        painter.drawPoint(pos.x(), pos.y())
        painter.end()
        self.setPixmap(self._pixmap)

class MyWindow(Qw.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello World")
        container = Canvas()
        self.setCentralWidget(container)


if __name__ == "__main__":
    app = Qw.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec()




