import math

"""
function spiral(n)
        k=ceil((sqrt(n)-1)/2)
        t=2*k+1
        m=t^2
        t=t-1
        if n>=m-t then return k-(m-n),-k        else m=m-t end
        if n>=m-t then return -k,-k+(m-n)       else m=m-t end
        if n>=m-t then return -k+(m-n),k else return k,k-(m-n-t) end
end
"""

## https://upload.wikimedia.org/wikipedia/commons/1/1d/Ulam_spiral_howto_all_numbers.svg

def move_r( pos ):
    x, y = pos
    return x+1, y

def move_l( pos ):
    x, y = pos
    return x-1, y

def move_u( pos ):
    x, y = pos
    return x, y+1

def move_d( pos ):
    x, y = pos
    return x, y-1

moves = [move_r, move_u, move_l, move_d]

def generate_moves():
    while True:
        for move in moves:
            yield move

def generate_spiral(n):
    pos = (0, 0)
    yield pos
    g = generate_moves()
    for i in range(n):
        for _ in ([0, 1]):
            move = next(g)
            for _ in range(i+1):
                pos = move(pos)
                yield pos


if __name__ == "__main__":
    i = 0
    for pos in generate_spiral(100):
        print(pos)
        i += 1
        if i > 100:
            break

