import PySide6.QtWidgets as Qw
import sys

class MyWindow(Qw.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello World")

        button = Qw.QPushButton("Click me")
        self.setCentralWidget(button)

        button.clicked.connect(self.on_button_clicked)

    def on_button_clicked(self):
        print("Button clicked")


if __name__ == "__main__":
    app = Qw.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec()




