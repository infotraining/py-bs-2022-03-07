#TODO
#using canvas -> draw shapes -> evolve

import PySide6.QtWidgets as Qw
import PySide6.QtGui as Qg
from PySide6.QtCore import Qt
import PySide6.QtCore as Qc

import sys
import time
from random import randint, choice
from spiral import generate_spiral

def is_prime(n):
    if n < 2:
        return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

class Canvas(Qw.QLabel):
    def __init__(self):
        super().__init__()
        self._pixmap = Qg.QPixmap(500, 500)
        self._pixmap.fill(Qt.white)
        self.setPixmap(self._pixmap)
        self.pen_color = Qg.QColor.black
        self.last_x = None
        self.last_y = None

    def update_from_array(self, array):
        self._pixmap.fill(Qt.white)
        painter = Qg.QPainter(self._pixmap)
        p = painter.pen()
        p.setWidth(1)
        for x in range(len(array)):
            for y in range(len(array[x])):
                if array[x][y] == 0:
                    p.setColor(Qg.QColor(255, 255, 255))
                else:
                    p.setColor(Qg.QColor(0, 0, 0))
                painter.setPen(p)
                painter.drawPoint(x, y)
        painter.end()
        self.setPixmap(self._pixmap)

class MainWindow(Qw.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Phonebook App")

        self.btn_run = Qw.QPushButton(text="Run")
        self.btn_exit = Qw.QPushButton(text="Exit")

        self.canvas = Canvas()

        layout = Qw.QVBoxLayout()

        layout.addWidget(self.canvas)

        h = Qw.QHBoxLayout()

        h.addWidget(self.btn_run)
        h.addStretch(1)
        h.addWidget(self.btn_exit)

        layout.addStretch(1)
        layout.addLayout(h)

        widget = Qw.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        self.btn_run.clicked.connect(self.create_spiral)
        self.btn_exit.clicked.connect(self.close)

        self.array = [[0 for x in range(500)] for y in range(500)]

    def create_spiral(self):
        for i, pos in enumerate(generate_spiral(1000)):
            x, y = pos
            x = x+250
            y = y+250
            if is_prime(i):
                self.array[x][y] = 1
            else:
                self.array[x][y] = 0
            if i > 100000:
                break
        self.canvas.update_from_array(self.array)


app = Qw.QApplication(sys.argv)
window = MainWindow()
window.show()

app.exec()