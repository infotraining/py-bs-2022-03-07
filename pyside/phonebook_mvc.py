import PySide6.QtWidgets as Qw
import PySide6.QtCore as Qc
import sys

class Entry(Qw.QDialog):
    def __init__(self):
        super().__init__()
        ## set editor layout
        ## QLabel
        ## QLineEdit
        layout = Qw.QVBoxLayout()
        self.setLayout(layout)

        self.name_label = Qw.QLabel("Name:")
        self.name_edit = Qw.QLineEdit()
        self.name_label.setBuddy(self.name_edit)
        self.phone_label = Qw.QLabel("Phone:")
        self.phone_edit = Qw.QLineEdit()
        self.phone_label.setBuddy(self.phone_edit)

        line1 = Qw.QHBoxLayout()
        line1.addWidget(self.name_label)
        line1.addWidget(self.name_edit)
        line2 = Qw.QHBoxLayout()
        line2.addWidget(self.phone_label)
        line2.addWidget(self.phone_edit)
        layout.addLayout(line1)
        layout.addLayout(line2)

        self.setWindowTitle("Add/Edit Entry")

        # ok and cancel buttons + behavior
        Qbtn = Qw.QDialogButtonBox.Ok | Qw.QDialogButtonBox.Cancel
        buttonBox = Qw.QDialogButtonBox(Qbtn)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        layout.addWidget(buttonBox)

class PhoneBookModel(Qc.QAbstractListModel):
    def __init__(self):
        super().__init__()
        self.phones = []

    def addEntry(self, data):
        self.phones.append(data)
        self.layoutChanged.emit()

    def removeEntry(self, selection):
        self.phones.pop(selection[0].row())
        self.layoutChanged.emit()

    def rowCount(self, index):
        return len(self.phones)

    def data(self, index, role):
        if role == Qc.Qt.DisplayRole:
            return self.phones[index.row()]

class MyWindow(Qw.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello World")
        container = Qw.QWidget()
        self.setCentralWidget(container)
        layout = Qw.QVBoxLayout()
        container.setLayout(layout)

        self.listwidget = Qw.QListView()
        self.model = PhoneBookModel()
        self.listwidget.setModel(self.model)

        button_add = Qw.QPushButton("Add")
        button_remove = Qw.QPushButton("Remove")
        button_load = Qw.QPushButton("Load")
        layout.addWidget(self.listwidget)
        line1 = Qw.QHBoxLayout()
        line1.addStretch(1)
        line1.addWidget(button_add)
        line1.addWidget(button_remove)
        line1.addWidget(button_load)
        layout.addLayout(line1)

        button_add.clicked.connect(self.on_add_button_clicked)
        button_remove.clicked.connect(self.on_remove_button_clicked)
        button_load.clicked.connect(self.on_load_button_clicked)

    def on_add_button_clicked(self):
        e = Entry()
        res = e.exec()
        if res == Qw.QDialog.Accepted:
            name = e.name_edit.text()
            phone = e.phone_edit.text()
            self.model.addEntry(f"{name} --- {phone}")

    def on_remove_button_clicked(self):
        sel = self.listwidget.selectedIndexes()
        if sel:
            self.model.removeEntry(sel)

    def on_load_button_clicked(self):
        filename, _ = Qw.QFileDialog.getOpenFileName(self, "Open File", "", "Text Files (*.txt)")
        self.listwidget.clear()
        with open(filename, "r") as f:
            for line in f:
                self.listwidget.addItem(line.strip())

if __name__ == "__main__":
    app = Qw.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec()




