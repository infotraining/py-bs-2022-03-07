import PySide6.QtWidgets as Qw
import PySide6.QtCore as Qc
import sys

class MyWindow(Qw.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello World")
        container = Qw.QWidget()
        self.setCentralWidget(container)
        layout = Qw.QVBoxLayout()
        container.setLayout(layout)

        slider = Qw.QSlider(Qc.Qt.Horizontal)
        dial = Qw.QDial()
        layout.addWidget(slider)
        #layout.addStretch()
        layout.addWidget(dial)

        slider.valueChanged.connect(dial.setValue)
        slider.valueChanged.connect(self.react)
        dial.valueChanged.connect(slider.setValue)

    def react(value):
        print("value: ", value)


if __name__ == "__main__":
    app = Qw.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec()




