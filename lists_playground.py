names = ["Ala", "Olga", "Kasia", "Piotr", "Kamil"]

# print names ending with a

print("*********")
for name in names:
    if name[-1] == "a":
        print(name)

# print names starting with K
print("*********")
for name in names:
    if name[0] == "K":
        print(name)

print("*********")
# remove "Olga" from list
names.remove("Olga")
#del names[1]
print(names)
# sort list
names.sort()
print(names)
# add to the list second list:

additional_names = ["leszek", "stefan", "Georgina", "Łukasz"]
names.extend(additional_names)

# sort combined list by the length of the names
names.sort()
for name in names:
    print(name)

names.sort(key=str.lower)
print(names)
names.sort(key=len)

print(names)