d = {"one": 1, "two": 2}
d2 = {}
print(d["one"])
d["three"] = 3
d["two"] = 4
#print(d["five"])
print(d)

for key in d:
    print(key, ":", d[key])

for key, value in d.items():
    print(key, ":", value)
