l = "Costam"
print(type(l))
print(id(l))
print(l)

a = str("Costam")
print(id(a)) # optimalization - same id as l
b = str(123)
print(b)
c = float("0.333")
print(type(c))
print(c)

class MyClass:
    def __init__(self, value="value") -> None:
        self.atr = value

    def my_method(self):
        print("Hello: ", self.atr)

instance = MyClass()
print(type(instance))
inst2 = MyClass()
print(id(instance))
print(id(inst2))
print(id(MyClass))
print(type(MyClass))

print("********************")
inst2.atr = "new value"
instance.my_method()
inst2.my_method()
MyClass.my_method(inst2)

l = [inst2, instance, MyClass("brand new value")]
for inst in l:
    inst.my_method()

l[2].atr = "once again new value"

for inst in l:
    inst.my_method()


## advanced - class modifications
# instance.a = 1
# print(instance.a)
# print(inst2.a)
