name = "genowefa"
long = """This is a very long string
that spans multiple lines"""

string_with_special = "Leszek\nTarkowski"

raw_string = r"Leszek\nTarkowski"

print(string_with_special)
print(raw_string)

print(len(long))
print(name.capitalize())
print(name.upper())
print(name[5:])
# name[3] = "a" - not working
if name.endswith("efa"):
    print(True)

if name.startswith("geno"):
    print(True)

l = long.split()
print(l)

l2 = name.split("e")
print(l2)  #['g', 'now', 'fa']

l3 = long.splitlines()
print(l3)

print("This is {} speaking".format(name))

print("Ala ma {1} a nie {0}".format(name, name.capitalize()))

print("Mam na imię {name} {surname}".format(name="Leszek", surname="Tarkowski"))

print("Mam na imię {name}".format(name=name))
print(f"Mam na imię {name}")
