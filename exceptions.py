a = 4
l = []

def div(a, b):
    res = a / b
    print("Log: division happened")
    return res

try:
    #div(a, 0)
    #l[0] = 123
    print("Not visible")

except ZeroDivisionError:
    print("Division by zero error")

except IndexError:
    print("Index error - accessing wrong index")

else:
    print("No exceptions")

finally:
    print("This will be printed no matter what")

l.append("cos")

print("Program continues")