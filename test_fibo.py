from fibo import fibonacci
import pytest

def test_return_list_for_fibonacci():
    assert isinstance(fibonacci(1), list)

def test_return_one_for_one():
    assert fibonacci(1) == [1]

def test_return_one_two_for_two():
    assert fibonacci(2) == [1, 1]

def test_return_list_for_7():
    assert fibonacci(7) == [1, 1, 2, 3, 5, 8, 13]

def test_for_negative_input():
    with pytest.raises(ValueError):
        fibonacci(-1)
