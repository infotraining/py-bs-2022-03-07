# https://bitbucket.org/infotraining/py-bs-2022-03-07

print("Hello World!")

l = 5
a = "Leszek"
name = a
print(name, a)
print(id(a))
print(id(name))
print(id(l))
l += 1
print("*******")
print("id:", id(l))
print("type:", type(l))
print("value:", l)

print("*******")
print("id:", id(a))
print("type:", type(a))
print("value:", a)

print("True division")
print(10 / 3)
print(10 // 3)

print(2 ** 129)

logic = 1

if logic:
    print("true")
else:
    print("not true")

a = 5 if logic else 10
print(a)

# for loop

a = "Hello"
for letter in a:
    print(letter)

for i in range(10):
    print(i)

#closest to c++/C#/C loop
for i, letter in enumerate(a):
    print(i, letter)